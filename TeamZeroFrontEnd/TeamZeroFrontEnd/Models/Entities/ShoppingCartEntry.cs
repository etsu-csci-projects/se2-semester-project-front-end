﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TeamZeroFrontEnd.Models.Entities
{
	public class ShoppingCartEntry
	{
		[Key]
		public int Id { get; set; }
		public string IdentityUserID { get; set; }
		public int BicycleSerialNumber { get; set; }
		public int Amount { get; set; }

		public ShoppingCartEntry(string IdentityUserID, int BicycleSerialNumber, int amount = 1)
		{
			this.IdentityUserID = IdentityUserID;
			this.BicycleSerialNumber = BicycleSerialNumber;
			this.Amount = amount;
		}

	}
}
