﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TeamZeroFrontEnd.Models.Entities
{
    public class ApplicationUser : IdentityUser
    {
        public string Address { get; set; }
        public double BalanceDue { get; set; }
        public double CityID { get; set; }
        public double CustomerID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public string ZIPCode { get; set; }
        /*
        public string ShoppingCart { get; set; }

        public void AddToCart(int bicycleID)
        {
            if (ShoppingCart.Length == 0)
                ShoppingCart = bicycleID.ToString();
            else
                ShoppingCart += $",{bicycleID}";
        }
        */
    }
}
