namespace TeamZeroFrontEnd.Module
{
	public class ComponentViewModel
	{
	        public string Category { get; set; }
	        public decimal ComponentID { get; set; }
	        public string Description { get; set; }
	        public decimal EndYear { get; set; }
	        public decimal EstimatedCost { get; set; }
	        public double Height { get; set; }
	        public double Length { get; set; }
	        public decimal ListPrice { get; set; }
	        public decimal ManufacturerID { get; set; }
	        public string ProductNumber { get; set; }
	        public double QuantityOnHand { get; set; }
	        public string Road { get; set; }
	        public double Weight { get; set; }
	        public double Width { get; set; }
	        public decimal Year { get; set; }
	}
}