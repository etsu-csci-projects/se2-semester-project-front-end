namespace TeamZeroFrontEnd.Module
{
	public class ModelSizeViewModel
	{
			public string ModelType { get; set; }
			public double MSize { get; set; }
			public double TopTube { get; set; }
			public double ChainStay { get; set; }
			public double TotalLength { get; set; }
			public double GroundClearance { get; set; }
			public double HeadTubeAngle { get; set; }
			public double SeatTubeAngle { get; set; }
	}
}