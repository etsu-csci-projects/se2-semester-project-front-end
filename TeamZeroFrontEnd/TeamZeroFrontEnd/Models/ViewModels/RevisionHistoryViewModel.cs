using System;

namespace TeamZeroFrontEnd.Module
{
	public class RevisionHistoryViewModel
	{
	        public decimal ID { get; set; }
	        public string Version { get; set; }
	        public DateTime ChangeDate { get; set; }
	        public string Name { get; set; }
	        public string RevisionComments { get; set; }
	}
}