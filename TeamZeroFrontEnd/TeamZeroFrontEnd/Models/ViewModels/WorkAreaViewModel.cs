namespace TeamZeroFrontEnd.Module
{
	public class WorkAreaViewModel
	{
	        public string WorkArea { get; set; }
	        public string Description { get; set; }
	}
}