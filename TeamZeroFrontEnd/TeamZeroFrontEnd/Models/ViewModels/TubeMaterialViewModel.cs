namespace TeamZeroFrontEnd.Module
{
	public class TubeMaterialViewModel
	{
	        public decimal TubeID { get; set; }
	        public string Material { get; set; }
	        public string Description { get; set; }
	        public double Diameter { get; set; }
	        public double Thickness { get; set; }
	        public string Roundness { get; set; }
	        public double Weight { get; set; }
	        public double Stiffness { get; set; }
	        public decimal ListPrice { get; set; }
	        public string Construction { get; set; }
	}
}