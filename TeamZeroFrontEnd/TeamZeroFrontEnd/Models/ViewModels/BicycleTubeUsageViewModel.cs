﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TeamZeroFrontEnd.Models
{
	public class BicycleTubeUsageViewModel
	{
		public double Quantity { get; set; }

		public decimal SerialNumber { get; set; }

		public decimal TubeID { get; set; }
	}
}
