namespace TeamZeroFrontEnd.Module
{
	public class StateTaxRateViewModel
	{
	        public string State { get; set; }
	        public double TaxRate { get; set; }
	}
}