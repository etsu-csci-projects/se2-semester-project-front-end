using System;

namespace TeamZeroFrontEnd.Module
{
	public class BikePartsViewModel
	{
	        public decimal ComponentID { get; set; }
	        public DateTime DateInstalled { get; set; }
	        public decimal EmployeeID { get; set; }
	        public string Location { get; set; }
	        public decimal Quantity { get; set; }
	        public decimal SerialNumber { get; set; }
	        public decimal SubstituteID { get; set; }
	}
}