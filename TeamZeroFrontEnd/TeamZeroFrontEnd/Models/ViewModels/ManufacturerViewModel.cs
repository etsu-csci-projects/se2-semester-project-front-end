namespace TeamZeroFrontEnd.Module
{
	public class ManufacturerViewModel
	{
			public decimal ManufacturerID { get; set; }
			public string ManufacturerName { get; set; }
			public string ContactName { get; set; }
			public string Phone { get; set; }
			public string Address { get; set; }
			public string ZipCode { get; set; }
			public decimal CityID { get; set; }
			public decimal BalanceDue { get; set; }
	}
}