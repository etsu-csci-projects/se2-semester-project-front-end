namespace TeamZeroFrontEnd.Module
{
	public class SampleNameViewModel
	{
	        public decimal ID { get; set; }
	        public string LastName { get; set; }
	        public string FirstName { get; set; }
	        public string Gender { get; set; }
	}
}