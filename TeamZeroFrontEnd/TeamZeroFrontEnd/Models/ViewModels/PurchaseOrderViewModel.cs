using System;

namespace TeamZeroFrontEnd.Module
{
	public class PurchaseOrderViewModel
	{
	        public decimal PurchaseID { get; set; }
	        public decimal EmployeeID { get; set; }
	        public decimal ManufacturerID { get; set; }
	        public decimal TotalList { get; set; }
	        public decimal ShippingCost { get; set; }
	        public decimal Discount { get; set; }
	        public DateTime OrderDate{ get; set; }
	        public DateTime ReceiveDate { get; set; }
	        public decimal AmountDue { get; set; }
	}
}