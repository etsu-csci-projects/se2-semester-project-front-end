using System;

namespace TeamZeroFrontEnd.Module
{
	public class PaintViewModel
	{
			public decimal PaintID { get; set; }//PK
			public string ColorName { get; set; }
			public string ColorStyle { get; set; }
			public string ColorList { get; set; }
			public DateTime DateIntroduced { get; set; }
			public DateTime DateDiscontinued { get; set; }
	}
}