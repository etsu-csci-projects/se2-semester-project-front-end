using System;

namespace TeamZeroFrontEnd.Module
{
	public class ManufacturerTransactionViewModel
	{
			public decimal ManufacturerID { get; set; }
			public DateTime TransactionDate { get; set; }
			public decimal EmployeeID { get; set; }
			public decimal Amount { get; set; }
			public string Description { get; set; }
			public decimal Reference { get; set; }
	}
}