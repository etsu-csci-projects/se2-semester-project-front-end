using System;

namespace TeamZeroFrontEnd.Module
{
	public class EmployeeViewModel
	{
	        public decimal EmployeeID { get; set; }
			public string TaxPayerID { get; set; }
			public string LastName { get; set; }
			public string FirstName { get; set; }
			public string HomePhone { get; set; }
			public string Address { get; set; }
			public string ZipCode { get; set; }
			public decimal CityID { get; set; }
			public DateTime DateHired { get; set; }
			public DateTime DateReleased { get; set; }
			public decimal CurrentManager { get; set; }
			public decimal SalaryGrade { get; set; }
			public decimal Salary { get; set; }
			public string Title { get; set; }
			public string WorkArea { get; set; }
	}
}