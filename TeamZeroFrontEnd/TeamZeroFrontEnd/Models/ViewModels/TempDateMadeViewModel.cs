using System;

namespace TeamZeroFrontEnd.Module
{
	public class TempDateMadeViewModel
	{
	        public DateTime DateValue { get; set; }
	        public double MadeCount { get; set; }
	}
}