namespace TeamZeroFrontEnd.Module
{
	public class BikeTubesViewModel
	{
	        public double Length { get; set; }
	        public decimal SerialNumber { get; set; }
	        public decimal TubeID { get; set; }
	        public string TubeName { get; set; }
	}
}