namespace TeamZeroFrontEnd.Module
{
	public class PurchaseItemViewModel
	{
	        public decimal PurchaseID { get; set; }
	        public decimal ComponentID { get; set; }
	        public decimal PricePaid { get; set; }
	        public decimal Quantity { get; set; }
	        public decimal QuantityReceived { get; set; }
	}
}