using System;

namespace TeamZeroFrontEnd.Module
{
	public class CustomerTransactionViewModel
	{
	        public decimal Amount { get; set; }
	        public decimal CustomerID { get; set; }
	        public string Description { get; set; }
	        public decimal EmployeeID { get; set; }
	        public decimal Reference { get; set; }
	        public DateTime TransactionDate { get; set; }
	}
}