namespace TeamZeroFrontEnd.Module
{
	public class CityViewModel
	{
	        public string AreaCode { get; set; }
	        public string City { get; set; }
	        public decimal CityID { get; set; }
	        public string Country { get; set; }
	        public double Latitude { get; set; }
	        public double Longitude { get; set; }
	        public decimal Population1980 { get; set; }
	        public decimal Population1990 { get; set; }
	        public double PopulationCDF { get; set; }
	        public string State { get; set; }
	        public string ZipCode { get; set; }
	}
}