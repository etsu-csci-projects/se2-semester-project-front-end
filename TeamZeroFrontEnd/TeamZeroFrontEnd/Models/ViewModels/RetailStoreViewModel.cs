namespace TeamZeroFrontEnd.Module
{
	public class RetailStoreViewModel
	{
	        public decimal StoreID { get; set; }
	        public string StoreName { get; set; }
	        public string Phone { get; set; }
	        public string ContactFirstName { get; set; }
	        public string ContactLastName { get; set; }
	        public string Address { get; set; }
	        public string ZipCode { get; set; }
	        public decimal CityID { get; set; }
	}
}