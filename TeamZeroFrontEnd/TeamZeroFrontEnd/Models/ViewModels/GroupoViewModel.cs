namespace TeamZeroFrontEnd.Module
{
	public class GroupoViewModel
	{
			public decimal ComponentGroupID { get; set; }//PK
			public string GroupName { get; set; }
			public string BikeType { get; set; }
			public decimal Year { get; set; }
			public decimal EndYear { get; set; }
			public decimal Weight { get; set; }
	}
}