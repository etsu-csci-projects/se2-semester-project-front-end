namespace TeamZeroFrontEnd.Module
{
	public class CustomerViewModel
	{
	        public string Address { get; set; }
	        public decimal BalanceDue { get; set; }
	        public decimal CityID { get; set; }
	        public decimal CustomerID { get; set; }
	        public string FirstName { get; set; }
	        public string LastName { get; set; }
	        public string Phone { get; set; }
	        public string ZipCode { get; set; }
	}
}