﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TeamZeroFrontEnd.Models
{
	public class BicycleViewModel
	{
        [DisplayName("Chain Stay")]
        public double ChainStay { get; set; }

        [DisplayName("Component List")]
        public decimal ComponentList { get; set; }

        [DisplayName("Construction")]
        public string Construction { get; set; }

        [DisplayName("Customer Id")]
        public decimal CustomerID { get; set; }

        [DisplayName("Customer Name")]
        public string CustomName { get; set; }

        [DisplayName("Employee ID")]
        public decimal EmployeeID { get; set; }

        [DisplayName("Frame Assembler")]
        public decimal FrameAssembler { get; set; }

        [DisplayName("Frame Price")]
        public decimal FramePrice { get; set; }

        [DisplayName("Frame Size")]
        public double FrameSize { get; set; }

        [DisplayName("Head Tube Angle")]
        public double HeadTubeAngle { get; set; }

        [DisplayName("Letter Style Id")]
        public string LetterStyleID { get; set; }

        [DisplayName("List Price")]
        public decimal ListPrice { get; set; }

        [DisplayName("Model Type")]
        public string ModelType { get; set; }

        [DisplayName("Order Date")]
        public DateTime OrderDate { get; set; }

        [DisplayName("Painter")]
        public decimal Painter { get; set; }

        [DisplayName("Paint Id")]
        public decimal PaintID { get; set; }

        [DisplayName("Sale Price")]
        public decimal SalePrice { get; set; }

        [DisplayName("Sale State")]
        public string SaleState { get; set; }

        [DisplayName("Sales Tax")]
        public decimal SalesTax { get; set; }

        [DisplayName("Seat Tube Angle")]
        public double SeatTubeAngle { get; set; }

        [DisplayName("Serial Number")]
        [Key]
        public int SerialNumber { get; set; }

        [DisplayName("Ship Date")]
        public DateTime ShipDate { get; set; }

        [DisplayName("Ship Employee")]
        public decimal ShipEmployee { get; set; }

        [DisplayName("Ship Price")]
        public decimal ShipPrice { get; set; }

        [DisplayName("Start Date")]
        public DateTime StartDate { get; set; }

        [DisplayName("Store Id")]
        public decimal StoreID { get; set; }

        [DisplayName("Top Tube")]
        public double TopTube { get; set; }

        [DisplayName("Water Bottle Brazeons")]
        public decimal WaterBottleBrazeons { get; set; }




	}
}
