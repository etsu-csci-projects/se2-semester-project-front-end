﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TeamZeroFrontEnd.Migrations
{
    public partial class mig01 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "shoppingCartEntries",
                columns: table => new
                {
                    CustomerID = table.Column<double>(nullable: false),
                    BicycleSerialNumber = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "shoppingCartEntries");
        }
    }
}
