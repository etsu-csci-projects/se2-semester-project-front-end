﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TeamZeroFrontEnd.Migrations
{
    public partial class mig04 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "CustomerID",
                table: "shoppingCartEntries",
                nullable: true,
                oldClrType: typeof(double),
                oldType: "double");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<double>(
                name: "CustomerID",
                table: "shoppingCartEntries",
                type: "double",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}
