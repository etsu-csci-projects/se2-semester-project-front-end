﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TeamZeroFrontEnd.Migrations
{
    public partial class mig05 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CustomerID",
                table: "shoppingCartEntries");

            migrationBuilder.AddColumn<string>(
                name: "IdentityUserID",
                table: "shoppingCartEntries",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IdentityUserID",
                table: "shoppingCartEntries");

            migrationBuilder.AddColumn<string>(
                name: "CustomerID",
                table: "shoppingCartEntries",
                type: "longtext CHARACTER SET utf8mb4",
                nullable: true);
        }
    }
}
