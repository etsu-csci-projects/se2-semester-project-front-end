﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using TeamZeroFrontEnd.Models;
using TeamZeroFrontEnd.Module;

namespace TeamZeroFrontEnd.Services
{
	public static class APIRequestHandler
	{
		private static readonly HttpClient httpClient = new HttpClient();
		private const string URL = "http://68.183.9.29/api/";

		//note this method will only work with bicycle
		public static async Task<object> GetPageAsync(int pageNumber, int pageSize)
		{ 
			var content = await httpClient.GetAsync(URL + "bicycle/"+pageNumber+"/"+pageSize);
			bool success = content.IsSuccessStatusCode;
			if (success)
				return JsonConvert.DeserializeObject<List<BicycleViewModel>>(content.Content.ReadAsStringAsync().Result);
			return -1;
		}

		public static async Task<object> GetPageOrderByListPriceAsync(int pageNumber, int pageSize, bool ascending)
		{ 
			var content = await httpClient.GetAsync(URL + "bicycle/ListPrice/"+pageNumber+"/"+pageSize+"/"+ascending);
			bool success = content.IsSuccessStatusCode;
			if (success)
				return JsonConvert.DeserializeObject<List<BicycleViewModel>>(content.Content.ReadAsStringAsync().Result);
			return -1;
		}
		public static async Task<object> GetPageOrderByTypeAsync(int pageNumber, int pageSize, string type, bool ascending)
		{ 
			var content = await httpClient.GetAsync(URL + "bicycle/Type/"+pageNumber+"/"+pageSize+"/"+type+"/"+ascending);
			bool success = content.IsSuccessStatusCode;
			if (success)
				return JsonConvert.DeserializeObject<List<BicycleViewModel>>(content.Content.ReadAsStringAsync().Result);
			return -1;
		}

		public static async Task<object> GetAsync(string table)
		{
			table = table.ToLower().Trim();
			var content = await httpClient.GetAsync(URL + table);
			bool success = content.IsSuccessStatusCode;
			if (success)
			{
				switch (table)
				{
					case "bicycle":
						return JsonConvert.DeserializeObject<List<BicycleViewModel>>(content.Content.ReadAsStringAsync().Result);
					case "bicycletubeusage":
						return JsonConvert.DeserializeObject<List<BicycleTubeUsageViewModel>>(content.Content.ReadAsStringAsync().Result);
					case "bikeparts":
						return JsonConvert.DeserializeObject<List<BikePartsViewModel>>(content.Content.ReadAsStringAsync().Result);
					case "biketubes":
						return JsonConvert.DeserializeObject<List<BikeTubesViewModel>>(content.Content.ReadAsStringAsync().Result);
					case "city":
						return JsonConvert.DeserializeObject<List<CityViewModel>>(content.Content.ReadAsStringAsync().Result);
					case "commonsizes":
						return JsonConvert.DeserializeObject<List<CommonSizesViewModel>>(content.Content.ReadAsStringAsync().Result);
					case "component":
						return JsonConvert.DeserializeObject<List<ComponentViewModel>>(content.Content.ReadAsStringAsync().Result);
					case "customer":
						return JsonConvert.DeserializeObject<List<CustomerViewModel>>(content.Content.ReadAsStringAsync().Result);
					case "customertransaction":
						return JsonConvert.DeserializeObject<List<CustomerTransactionViewModel>>(content.Content.ReadAsStringAsync().Result);
					case "employee":
						return JsonConvert.DeserializeObject<List<EmployeeViewModel>>(content.Content.ReadAsStringAsync().Result);
					case "groupcomponents":
						return JsonConvert.DeserializeObject<List<GroupComponentsViewModel>>(content.Content.ReadAsStringAsync().Result);
					case "groupo":
						return JsonConvert.DeserializeObject<List<GroupoViewModel>>(content.Content.ReadAsStringAsync().Result);
					case "manufacturer":
						return JsonConvert.DeserializeObject<List<ManufacturerViewModel>>(content.Content.ReadAsStringAsync().Result);
					case "manufacturertransaction":
						return JsonConvert.DeserializeObject<List<ManufacturerTransactionViewModel>>(content.Content.ReadAsStringAsync().Result);
					case "modelsize":
						return JsonConvert.DeserializeObject<List<ModelSizeViewModel>>(content.Content.ReadAsStringAsync().Result);
					case "paint":
						return JsonConvert.DeserializeObject<List<PaintViewModel>>(content.Content.ReadAsStringAsync().Result);
					case "purchaseitem":
						return JsonConvert.DeserializeObject<List<PurchaseItemViewModel>>(content.Content.ReadAsStringAsync().Result);
					case "purchaseorder":
						return JsonConvert.DeserializeObject<List<PurchaseOrderViewModel>>(content.Content.ReadAsStringAsync().Result);
					case "retailstore":
						return JsonConvert.DeserializeObject<List<RetailStoreViewModel>>(content.Content.ReadAsStringAsync().Result);
					case "revisionhistory":
						return JsonConvert.DeserializeObject<List<RevisionHistoryViewModel>>(content.Content.ReadAsStringAsync().Result);
					case "samplename":
						return JsonConvert.DeserializeObject<List<SampleNameViewModel>>(content.Content.ReadAsStringAsync().Result);
					case "samplestreet":
						return JsonConvert.DeserializeObject<List<SampleStreetViewModel>>(content.Content.ReadAsStringAsync().Result);
					case "statetaxrate":
						return JsonConvert.DeserializeObject<List<StateTaxRateViewModel>>(content.Content.ReadAsStringAsync().Result);
					case "tempdatemade":
						return JsonConvert.DeserializeObject<List<TempDateMadeViewModel>>(content.Content.ReadAsStringAsync().Result);
					case "tubematerial":
						return JsonConvert.DeserializeObject<List<TubeMaterialViewModel>>(content.Content.ReadAsStringAsync().Result);
					case "workarea":
						return JsonConvert.DeserializeObject<List<WorkAreaViewModel>>(content.Content.ReadAsStringAsync().Result);
					default:
						return -1;
				}
			}
			return -1;
		}

		public static async Task<object> GetAsync(string table, int id)
		{

			table = table.ToLower().Trim();
			var content = await httpClient.GetAsync(URL + table + $"/{id}");
			bool success = content.IsSuccessStatusCode;

			if (success)
			{
				switch (table)
				{
					case "bicycle":
						return JsonConvert.DeserializeObject<BicycleViewModel>(content.Content.ReadAsStringAsync().Result);
					case "bicycletubeusage":
						return JsonConvert.DeserializeObject<BicycleTubeUsageViewModel>(content.Content.ReadAsStringAsync().Result);
					case "bikeparts":
						return JsonConvert.DeserializeObject<BikePartsViewModel>(content.Content.ReadAsStringAsync().Result);
					case "biketubes":
						return JsonConvert.DeserializeObject<BikeTubesViewModel>(content.Content.ReadAsStringAsync().Result);
					case "city":
						return JsonConvert.DeserializeObject<CityViewModel>(content.Content.ReadAsStringAsync().Result);
					case "commonsizes":
						return JsonConvert.DeserializeObject<CommonSizesViewModel>(content.Content.ReadAsStringAsync().Result);
					case "component":
						return JsonConvert.DeserializeObject<ComponentViewModel>(content.Content.ReadAsStringAsync().Result);
					case "customer":
						return JsonConvert.DeserializeObject<CustomerViewModel>(content.Content.ReadAsStringAsync().Result);
					case "customertransaction":
						return JsonConvert.DeserializeObject<CustomerTransactionViewModel>(content.Content.ReadAsStringAsync().Result);
					case "employee":
						return JsonConvert.DeserializeObject<EmployeeViewModel>(content.Content.ReadAsStringAsync().Result);
					case "groupcomponents":
						return JsonConvert.DeserializeObject<GroupComponentsViewModel>(content.Content.ReadAsStringAsync().Result);
					case "groupo":
						return JsonConvert.DeserializeObject<GroupoViewModel>(content.Content.ReadAsStringAsync().Result);
					case "manufacturer":
						return JsonConvert.DeserializeObject<ManufacturerViewModel>(content.Content.ReadAsStringAsync().Result);
					case "manufacturertransaction":
						return JsonConvert.DeserializeObject<ManufacturerTransactionViewModel>(content.Content.ReadAsStringAsync().Result);
					case "modelsize":
						return JsonConvert.DeserializeObject<ModelSizeViewModel>(content.Content.ReadAsStringAsync().Result);
					case "paint":
						return JsonConvert.DeserializeObject<PaintViewModel>(content.Content.ReadAsStringAsync().Result);
					case "purchaseitem":
						return JsonConvert.DeserializeObject<PurchaseItemViewModel>(content.Content.ReadAsStringAsync().Result);
					case "purchaseorder":
						return JsonConvert.DeserializeObject<PurchaseOrderViewModel>(content.Content.ReadAsStringAsync().Result);
					case "retailstore":
						return JsonConvert.DeserializeObject<RetailStoreViewModel>(content.Content.ReadAsStringAsync().Result);
					case "revisionhistory":
						return JsonConvert.DeserializeObject<RevisionHistoryViewModel>(content.Content.ReadAsStringAsync().Result);
					case "samplename":
						return JsonConvert.DeserializeObject<SampleNameViewModel>(content.Content.ReadAsStringAsync().Result);
					case "samplestreet":
						return JsonConvert.DeserializeObject<SampleStreetViewModel>(content.Content.ReadAsStringAsync().Result);
					case "statetaxrate":
						return JsonConvert.DeserializeObject<StateTaxRateViewModel>(content.Content.ReadAsStringAsync().Result);
					case "tempdatemade":
						return JsonConvert.DeserializeObject<TempDateMadeViewModel>(content.Content.ReadAsStringAsync().Result);
					case "tubematerial":
						return JsonConvert.DeserializeObject<TubeMaterialViewModel>(content.Content.ReadAsStringAsync().Result);
					case "workarea":
						return JsonConvert.DeserializeObject<WorkAreaViewModel>(content.Content.ReadAsStringAsync().Result);
					default:
						return -1;
				}
			}
			else
				return -1;

		}

		public static async Task<bool> PostAsync(string table, string jsonContent)
		{
			table = table.ToLower().Trim();
			var content = new StringContent(jsonContent, Encoding.UTF8, "application/json");
			HttpResponseMessage r = await httpClient.PostAsync(URL + table, content);
			return r.IsSuccessStatusCode;
		}

		public static async Task<bool> DeleteAsync(string table)
		{
			table = table.ToLower().Trim();
			var result = await httpClient.DeleteAsync(URL + table);
			return result.IsSuccessStatusCode;
		}

		public static async Task<bool> DeleteAsync(string table, int id)
		{
			table = table.ToLower().Trim();
			var result = await httpClient.DeleteAsync(URL + table + $"/{id}");
			return result.IsSuccessStatusCode;
		}

		public static async Task<bool> UpdateAsync(string table, int id, string jsonContent)
		{
			table = table.ToLower().Trim();
			var content = new StringContent(jsonContent, Encoding.UTF8, "application/json");
			var result = await httpClient.PutAsync(URL + table + $"/{id}", content);
			return result.IsSuccessStatusCode;
		}

		public static string SerializeObject(object obj)
		{
			return JsonConvert.SerializeObject(obj);
		}



	}
}
