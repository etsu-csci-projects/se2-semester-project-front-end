﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TeamZeroFrontEnd.Models.Entities;

namespace TeamZeroFrontEnd.Services
{
	public class DbShoppingCartRepository : IShoppingCartRepository
	{
		private ApplicationDbContext _db;
		public DbShoppingCartRepository(ApplicationDbContext db)
		{
			_db = db;
		}
		public List<ShoppingCartEntry> Read(string ID)
		{
			return _db.shoppingCartEntries.Where(sce => sce.IdentityUserID == ID).ToList();
		}

		public void AddToCart(ShoppingCartEntry shoppingCartEntry)
		{
			if (_db.shoppingCartEntries.Any(e => (e.BicycleSerialNumber == shoppingCartEntry.BicycleSerialNumber && e.IdentityUserID == shoppingCartEntry.IdentityUserID)))
			{ 
				var entry = _db.shoppingCartEntries.FirstOrDefault(sce => sce.BicycleSerialNumber == shoppingCartEntry.BicycleSerialNumber);
				entry.Amount++;
			}
			else
				_db.shoppingCartEntries.Add(shoppingCartEntry);
			_db.SaveChanges();
		}

		public void RemoveFromCart(ShoppingCartEntry shoppingCartEntry)
		{
			var entryToRemove = _db.shoppingCartEntries.FirstOrDefault(e => e.BicycleSerialNumber == shoppingCartEntry.BicycleSerialNumber && e.IdentityUserID == shoppingCartEntry.IdentityUserID);
			_db.Remove(entryToRemove);
			_db.SaveChanges();
		}

		public void ClearCart()
		{
			_db.shoppingCartEntries.RemoveRange(_db.shoppingCartEntries);
			_db.SaveChanges();
		}

		public void AddOne(ShoppingCartEntry shoppingCartEntry)
		{ 
			var entryEdit = _db.shoppingCartEntries.FirstOrDefault(e => e.BicycleSerialNumber == shoppingCartEntry.BicycleSerialNumber && e.IdentityUserID == shoppingCartEntry.IdentityUserID);
			entryEdit.Amount++;
			_db.SaveChanges();
		}
		public void RemoveOne(ShoppingCartEntry shoppingCartEntry)
		{ 
			var entryEdit = _db.shoppingCartEntries.FirstOrDefault(e => e.BicycleSerialNumber == shoppingCartEntry.BicycleSerialNumber && e.IdentityUserID == shoppingCartEntry.IdentityUserID);
			if (entryEdit.Amount == 1)
				_db.Remove(entryEdit);
			else
				entryEdit.Amount--;
			_db.SaveChanges();
		}
	}
}
