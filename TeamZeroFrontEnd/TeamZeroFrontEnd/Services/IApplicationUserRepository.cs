﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TeamZeroFrontEnd.Models.Entities;

namespace TeamZeroFrontEnd.Services
{
    public interface IApplicationUserRepository
    {
        ApplicationUser Read(string userName);
    }
}
