﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TeamZeroFrontEnd.Models.Entities;

namespace TeamZeroFrontEnd.Services
{
    public class DbApplicationUserRepository : IApplicationUserRepository
    {
        private ApplicationDbContext _db;

        public DbApplicationUserRepository(ApplicationDbContext db)
        {
            _db = db;
        }

        public ApplicationUser Read(string userName)
        {
            return _db.Users.FirstOrDefault(u => u.UserName == userName);
        }
    }
}
