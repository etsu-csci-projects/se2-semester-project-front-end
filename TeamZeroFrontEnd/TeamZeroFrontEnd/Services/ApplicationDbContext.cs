﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using TeamZeroFrontEnd.Models.Entities;
using TeamZeroFrontEnd.Models;
using Microsoft.AspNetCore.Identity;

namespace TeamZeroFrontEnd.Services
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser, IdentityRole, string>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
        public DbSet<ShoppingCartEntry> shoppingCartEntries { get; set; }

		protected override void OnModelCreating(ModelBuilder builder)
		{
            base.OnModelCreating(builder);
		}
		//public DbSet<TeamZeroFrontEnd.Models.BicycleViewModel> BicycleViewModel { get; set; }
	}
}
