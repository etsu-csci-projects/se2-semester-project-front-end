﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TeamZeroFrontEnd.Models.Entities;

namespace TeamZeroFrontEnd.Services
{
	public interface IShoppingCartRepository
	{
		List<ShoppingCartEntry> Read(string CustomerID);
		void AddToCart(ShoppingCartEntry shoppingCartEntry);
		void RemoveFromCart(ShoppingCartEntry shoppingCartEntry);
		void ClearCart();
		void AddOne(ShoppingCartEntry shoppingCartEntry);
		void RemoveOne(ShoppingCartEntry shoppingCartEntry);

	}
}
