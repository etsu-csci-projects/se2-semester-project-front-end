﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using TeamZeroFrontEnd.Models;
using TeamZeroFrontEnd.Services;

namespace TeamZeroFrontEnd.Controllers
{
	[Authorize(Roles = "Admin")]
	public class BicycleController : Controller
	{
		public async Task<IActionResult> IndexAsync()
		{
			List<BicycleViewModel> model = await APIRequestHandler.GetAsync("bicycle") as List<BicycleViewModel>;
			if(model != null)
				return View(model);
			return View("~/Views/Home/Index.cshtml");
		}

		//GET: /bicycle/create
		[HttpGet]
		public IActionResult Create()
		{
			return View();
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> Create([Bind] BicycleViewModel bicycle)
		{
			if (ModelState.IsValid)
			{
				var jsonContent = APIRequestHandler.SerializeObject(bicycle);
				var result = await APIRequestHandler.PostAsync("bicycle", jsonContent);
				if (result)
					return RedirectToAction("Index");
			}
			return View(bicycle);
		}


		[HttpGet]
		public async Task<IActionResult> DeleteAsync(int id)
		{
			BicycleViewModel model = await APIRequestHandler.GetAsync("bicycle", id) as BicycleViewModel;
			if(model != null)
				return View(model);
			return NotFound();
		}

		[HttpPost, ActionName("Delete")]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> DeleteConfirmedAsync(int id)
		{
			var result = await APIRequestHandler.DeleteAsync("bicycle", id);
			if (result)
				return RedirectToAction("Index");
			return View("~/Views/Shared/Erorr.cshtml");
		}

		[HttpGet]
		public async Task<IActionResult> EditAsync(int id)
		{
			BicycleViewModel model = await APIRequestHandler.GetAsync("bicycle", id) as BicycleViewModel;
			if(model != null)
				return View(model);
			return NotFound();
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> EditAsync(int id, [Bind]BicycleViewModel bicycle)
		{
			var content = APIRequestHandler.SerializeObject(bicycle);
			var result = await APIRequestHandler.UpdateAsync("bicycle", id, content);
			if (result)
				return RedirectToAction("Index");
			return View("~/Views/Shared/Erorr.cshtml");
		}

		[HttpGet]
		public async Task<IActionResult> Details(int id)
		{
			var bike = await APIRequestHandler.GetAsync("bicycle", id);
			if (bike != null)
				return View(bike);
			return NotFound();
		}

	}
}
