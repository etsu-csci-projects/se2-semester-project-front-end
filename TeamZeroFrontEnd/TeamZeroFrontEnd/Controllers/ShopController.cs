﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TeamZeroFrontEnd.Models;
using TeamZeroFrontEnd.Models.Entities;
using TeamZeroFrontEnd.Services;

namespace TeamZeroFrontEnd.Controllers
{
	[Authorize]
	[Route("[controller]")]
	[Controller]
	public class ShopController : Controller
	{
		private IShoppingCartRepository _cartRepo;
		private IApplicationUserRepository _userRepo;
		public ShopController(IShoppingCartRepository cartRepo, IApplicationUserRepository userRepo)
		{
			_cartRepo = cartRepo;
			_userRepo = userRepo;
		}

		[HttpGet("{pageNumber}")]
		public async Task<IActionResult> IndexAsync(int pageNumber = 1)
		{
			List<BicycleViewModel> model = await APIRequestHandler.GetPageAsync(pageNumber, 30) as List<BicycleViewModel>;
			if (model != null)
				return View(model);
			return View("~/Views/Home/Index.cshtml");
		}

		[HttpGet("{pageNumber}/{ascending}")]
		public async Task<IActionResult> IndexAsync(bool ascending, int pageNumber = 1)
		{ 
			List<BicycleViewModel> model = await APIRequestHandler.GetPageOrderByListPriceAsync(pageNumber, 30, ascending) as List<BicycleViewModel>;
			if (model != null)
				return View(model);
			return View("~/Views/Home/Index.cshtml");
		}

		[HttpGet("{pageNumber}/{type}/{ascending}")]
		public async Task<IActionResult> IndexAsync(string type, bool ascending, int pageNumber = 1)
		{ 
			List<BicycleViewModel> model = await APIRequestHandler.GetPageOrderByTypeAsync(pageNumber, 30, type, ascending) as List<BicycleViewModel>;
			if (model != null)
				return View(model);
			return View("~/Views/Home/Index.cshtml");
		}

		[HttpGet("AddToCart/{SerialNumber}/{PageNumber}")]
		public IActionResult AddToCart(int SerialNumber, string PageNumber)
		{
			var currentUser = _userRepo.Read(User.Identity.Name);
			_cartRepo.AddToCart(new ShoppingCartEntry(currentUser.Id, SerialNumber));
			return Redirect($"~/Shop/{PageNumber}");
		}

		[HttpGet("Checkout")]
		public async Task<IActionResult> Checkout()
		{
			var currentUser = _userRepo.Read(User.Identity.Name);

			List<ShoppingCartEntry> cart = _cartRepo.Read(currentUser.Id);
			List<BicycleViewModel> bikes = new List<BicycleViewModel>();
			foreach (var item in cart)
			{
				BicycleViewModel result = await APIRequestHandler.GetAsync("bicycle", item.BicycleSerialNumber) as BicycleViewModel;
				bikes.Add(result);
			}
			var tuple = new Tuple<List<BicycleViewModel>, List<ShoppingCartEntry>>(bikes, cart);
			return View(tuple);

		}

		[HttpGet("ClearCart")]
		public IActionResult ClearCart()
		{
			_cartRepo.ClearCart();
			return Redirect("/Shop/ShoppingCart");
		}

		[HttpGet("BuyNow/{SerialNumber}")]
		public IActionResult BuyNow(int SerialNumber)
		{ 
			var currentUser = _userRepo.Read(User.Identity.Name);
			_cartRepo.AddToCart(new ShoppingCartEntry(currentUser.Id, SerialNumber));
			return Redirect("/Shop/ShoppingCart");
		}

		[HttpGet("RemoveFromCart/{SerialNumber}")]
		public IActionResult RemoveFromCart(int SerialNumber)
		{
			var currentUser = _userRepo.Read(User.Identity.Name);
			_cartRepo.RemoveFromCart(new ShoppingCartEntry(currentUser.Id, SerialNumber));
			return Redirect("/Shop/ShoppingCart");
		}

		[HttpGet("AddOne/{SerialNumber}")]
		public IActionResult AddOne(int SerialNumber)
		{ 
			var currentUser = _userRepo.Read(User.Identity.Name);
			_cartRepo.AddOne(new ShoppingCartEntry(currentUser.Id, SerialNumber));
			return Redirect("/Shop/ShoppingCart");
		}

		[HttpGet("RemoveOne/{SerialNumber}")]
		public IActionResult RemoveOne(int SerialNumber)
		{ 
			var currentUser = _userRepo.Read(User.Identity.Name);
			_cartRepo.RemoveOne(new ShoppingCartEntry(currentUser.Id, SerialNumber));
			return Redirect("/Shop/ShoppingCart");
		}

		[HttpGet("ShoppingCart")]
		public async Task<IActionResult> ShoppingCart()
		{
			var currentUser = _userRepo.Read(User.Identity.Name);

			List<ShoppingCartEntry> cart = _cartRepo.Read(currentUser.Id);
			List<BicycleViewModel> bikes = new List<BicycleViewModel>();
			foreach (var item in cart)
			{
				BicycleViewModel result = await APIRequestHandler.GetAsync("bicycle", item.BicycleSerialNumber) as BicycleViewModel;
				bikes.Add(result);
			}
			var tuple = new Tuple<List<BicycleViewModel>, List<ShoppingCartEntry>>(bikes, cart);
			return View(tuple);
		}
	}
}
